package com.example.weather.network;

import com.example.weather.model.Weather;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIManager {
    String API_KEY = "eSTe7nGyP6wDOYf5WUrydajLaNMOzUVz";
    String SERVER_URL = "http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/";
    String URL = "353412?apikey="+API_KEY+"&language=vi-vn&metric=true";

    @GET(URL)
    Call<List<Weather>> apiGetListData();


}
