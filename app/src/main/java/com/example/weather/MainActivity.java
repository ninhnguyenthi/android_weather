package com.example.weather;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;


import com.example.weather.adapter.HourAdapter;
import com.example.weather.model.Weather;
import com.example.weather.network.APIManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    TextView tvTemperature, tvIconPhrase;
    List<Weather> listWeather = new ArrayList<>();
    HourAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTemperature = findViewById(R.id.tv_temperature);
        tvIconPhrase = findViewById(R.id.tv_iconphrase);

        //B1: Data source
        getListData();

        //B2: Adapter
         adapter = new HourAdapter(this, listWeather);


        //B3: layout Manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL,false);



        //B4: Recycleview
        RecyclerView recyclerView = findViewById(R.id.rvTemperature);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    private void getListData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIManager service = retrofit.create(APIManager.class);
        service.apiGetListData().enqueue(new Callback<List<Weather>>() {
            @Override
            public void onResponse(Call<List<Weather>> call, Response<List<Weather>> response) {
                Log.d("TEST", "onResponse: " + response);
                if(response.body() != null){
                    listWeather = response.body();
                    Weather weatherFirstData = listWeather.get(0);
                    tvTemperature.setText(weatherFirstData.getTemperature().getValue().toString()+ "°");
                    tvIconPhrase.setText(weatherFirstData.getIconPhrase());
                    adapter.reloadData(listWeather);
//                    tv_version.setText(String.valueOf(firstdata.getVersion()));
                    //string to int
//                    Integer.parseInt()
                }

            }

            @Override
            public void onFailure(Call<List<Weather>> call, Throwable t) {
                Log.d("TEST", "onFailure: " + t);
            }
        });
    }



}