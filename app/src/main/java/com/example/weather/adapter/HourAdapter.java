package com.example.weather.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.weather.R;
import com.example.weather.model.Weather;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class HourAdapter extends RecyclerView.Adapter {
    private Activity activity;
    private List<Weather> listWeather;

    public HourAdapter(Activity activity, List<Weather> listWeather) {
        this.activity = activity;
        this.listWeather = listWeather;
    }

    public void reloadData(List<Weather> list){
        this.listWeather = list;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.item_weather,parent,false);
        HourHolder hourHolder = new HourHolder(itemView);
        return hourHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        HourHolder hd = (HourHolder) holder;
        Weather model = listWeather.get(position);
        hd.tvTime.setText(convertTime(model.getDateTime()));
        hd.tvTemp.setText(model.getTemperature().getValue().toString());
        Glide.with(activity).load(convertUrlIcon(model.getWeatherIcon())).into(hd.icIcon);
    }

    @Override
    public int getItemCount() {
        return listWeather.size();
    }

    public class HourHolder extends RecyclerView.ViewHolder {
        TextView tvTime, tvTemp;
        ImageView icIcon;

        public HourHolder(@NonNull View itemView) {
            super(itemView);
            tvTime = itemView.findViewById(R.id.tv_time);
            icIcon = itemView.findViewById(R.id.ic_icon);
            tvTemp = itemView.findViewById(R.id.tv_temp);
        }
    }

    public String convertTime(String inputTime){
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = null;
        try{
            date = inFormat.parse(inputTime);
        }catch (ParseException e){
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("ha");
        String goal = outFormat.format(date);
        return goal;
    }

    public String convertUrlIcon(int input){
        String url = "https://developer.accuweather.com/sites/default/files/";
        String icon = "";
        if(input < 10){
            icon = "0"+input +"-s.png";
        }else{
            icon = input+"-s.png";
        }

        return url +icon;
    }


}
